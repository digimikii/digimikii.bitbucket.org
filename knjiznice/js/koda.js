/* global $, d3, btoa */

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var vitalniZnaki = [];

function narisiGraf(mesecniTlaki) {
  d3.select("svg").remove();
  
  var parseMonth = d3.timeParse("%b");
  var data = mesecniTlaki.map(function (d) {
             return {
               month: parseMonth(d.month),
               sys: d.sys,
               dia: d.dia,
               map: d.map
             };
  });
  
  var margin = {top: 20, right: 20, bottom: 50, left: 40},
                width = 590-margin.left-margin.right,
                height = 500-margin.top-margin.bottom;
  
  var x = d3.scaleTime().range([0, width]);
  var y = d3.scaleLinear().range([height, 0]);
  
  var lineSys = d3.line()
                .x(function(d) { return x(d.month); })
                .y(function(d) { return y(d.sys); });
  var lineDia = d3.line()
                .x(function(d) { return x(d.month); })
                .y(function(d) { return y(d.dia); });
  var map = d3.line()
            .x(function(d) { return x(d.month); })
            .y(function(d) { return y(d.map); });
            
  var normalSys = d3.line()
                 .x(function(d) { return x(d.month); })
                 .y(function() { return y(120); });
  var normalDia = d3.line()
                 .x(function(d) { return x(d.month); })
                 .y(function() { return y(80); });
                 
  var mapArea = d3.area()
                .x(function(d) { return x(d.month); })
                .y0(function() { return y(70); })
                .y1(function() { return y(100); });
                 
  var svg = d3.select("#graf").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
      
  x.domain(d3.extent(data, function(d) { return d.month; }));
  y.domain([d3.min(data, function(d) { return Math.min(70, d.dia); })-3, d3.max(data, function(d) { return Math.max(120, d.sys); })+3]);
	
	svg.append("path")
	   .data([data])
     .attr("class", "line")
     .style("stroke", "#ffb2c3")
     .attr("d", lineSys);
    
  svg.append("path")
     .data([data])
     .attr("class", "line")
     .style("stroke", "#ff0039")
     .attr("d", lineDia);
     
  svg.append("path")
     .data([data])
     .attr("class", "line")
     .style("stroke", "#31aa31")
     .attr("d", map);
  
  svg.append("path")
     .data([data])
     .attr("class", "line")
     .style("stroke", "#ccefff")
     .attr("d", normalSys);
  
  svg.append("path")
     .data([data])
     .attr("class", "line")
     .style("stroke", "#2fbcfc")
     .attr("d", normalDia);
     
  svg.append("path")
    .data([data])
    .attr("class", "area")
    .attr("d", mapArea);
  
  svg.append("g")
     .attr("transform", "translate(0," + height + ")")
     .call(d3.axisBottom(x)
       .ticks(d3.timeMonth)
       .tickFormat(d3.timeFormat("%B")));
       
  svg.append("g")
     .call(d3.axisLeft(y));
}

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  var ehrId, ime, priimek, datumRojstva;
  if (stPacienta == 1) {
    ime = "Janez";
    priimek = "Loleks";
    datumRojstva = "1993-01-01";
  } else if (stPacienta == 2) {
    ime = "Janja";
    priimek = "Volk";
    datumRojstva = "1999-02-02";
  } else if (stPacienta == 3) {
    ime = "Metka";
    priimek = "Rožnik";
    datumRojstva = "1968-03-03";
  } else if (stPacienta == 4) {
    ime = $("#kreirajIme").val();
    priimek = $("#kreirajPriimek").val();
    datumRojstva = $("#kreirajLeto").val() + "-" + $("#kreirajMesec").val() + "-" + $("#kreirajDan").val();
  }
  
  if (stPacienta == 4 && (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0)) {
        $("#kreirajSporocilo").html("<span class='obvestilo label " +
        "label-warning'>Prosim vnesite zahtevane podatke!</span>");
  } else {
    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function(party) {
            if (party.action == 'CREATE') {
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success'>Uspešno kreiran EHR '" +
                ehrId + "'.</span>");
              $("#preberiEHRid").val(ehrId);
              if (stPacienta == 1) {
                $("#bolnik1").val(ehrId);
                $("#bolnik1").prop('disabled', false);
              } else if (stPacienta == 2) {
                $("#bolnik2").val(ehrId);
                $("#bolnik2").prop('disabled', false);
              } else if (stPacienta == 3) {
                $("#bolnik3").val(ehrId);
                $("#bolnik3").prop('disabled', false);
              }
            }
            if (stPacienta == 1) {
              dodajMeritve(ehrId, "2019-01-15T10:00", "165", "68.3", "36.1", "122", "81", "99", "Janez Loleks");
              dodajMeritve(ehrId, "2019-01-25T18:00", "165", "68.1", "36", "119", "80", "99", "Janez Loleks");
              
              dodajMeritve(ehrId, "2019-02-15T10:00", "165", "69.1", "36.3", "123", "80", "99", "Janez Loleks");
              dodajMeritve(ehrId, "2019-02-25T18:00", "165", "68.8", "36.1", "119", "80", "99", "Metka Rožnik");
              
              dodajMeritve(ehrId, "2019-03-15T10:00", "165", "68.9", "36.1", "120", "81", "100", "Janez Loleks");
              dodajMeritve(ehrId, "2019-03-25T18:00", "165", "68.6", "36", "118", "80", "99", "Janez Loleks");
              
              dodajMeritve(ehrId, "2019-04-15T10:00", "165", "68.5", "36", "124", "80", "100", "Janez Loleks");
              dodajMeritve(ehrId, "2019-04-25T18:00", "165", "68.5", "36.2", "120", "80", "98", "Janez Loleks");
              
              dodajMeritve(ehrId, "2019-05-15T10:00", "165", "68.7", "36.2", "120", "81", "99", "Janez Loleks");
              dodajMeritve(ehrId, "2019-05-25T18:00", "165", "68.4", "36.3", "120", "80", "98", "Janez Loleks");
            } else if (stPacienta == 2) {
              dodajMeritve(ehrId, "2018-06-15T10:00", "157", "53.3", "36.3", "120", "58", "95", "Janja Volk");
              dodajMeritve(ehrId, "2018-06-25T18:00", "157", "53.3", "36.2", "119", "58", "95", "Janja Volk");
              
              dodajMeritve(ehrId, "2018-07-15T10:00", "157", "53.2", "35.9", "118", "57", "95", "Janja Volk");
              dodajMeritve(ehrId, "2018-07-25T18:00", "157", "53.3", "36", "119", "58", "96", "Janja Volk");
              
              dodajMeritve(ehrId, "2018-08-15T10:00", "157", "52.8", "36.2", "120", "57", "94", "Janja Volk");
              dodajMeritve(ehrId, "2018-08-25T18:00", "157", "53", "36.2", "119", "56", "93", "Janja Volk");
              
              dodajMeritve(ehrId, "2018-09-15T10:00", "157", "52.6", "36.2", "118", "54", "94", "Janja Volk");
              dodajMeritve(ehrId, "2018-09-25T18:00", "157", "52.4", "36", "117", "55", "94", "Janja Volk");
              
              dodajMeritve(ehrId, "2018-11-15T10:00", "157", "52.3", "35.8", "117", "56", "93", "Janja Volk");
              dodajMeritve(ehrId, "2018-11-25T18:00", "157", "52.3", "36", "117", "57", "93", "Janja Volk");

            } else if (stPacienta == 3) {
              dodajMeritve(ehrId, "2018-03-15T10:00", "165", "85.3", "36.5", "153", "105", "95", "Metka Rožnik");
              dodajMeritve(ehrId, "2018-03-25T18:00", "165", "85.5", "36.3", "154", "105", "95", "Metka Rožnik");
              
              dodajMeritve(ehrId, "2018-05-15T10:00", "152", "87.8", "36.6", "156", "106", "95", "Metka Rožnik");
              dodajMeritve(ehrId, "2018-05-25T18:00", "152", "87.9", "36.5", "157", "105", "96", "Metka Rožnik");
              
              dodajMeritve(ehrId, "2018-08-15T10:00", "152", "90.9", "36.6", "159", "106", "94", "Metka Rožnik");
              dodajMeritve(ehrId, "2018-08-25T18:00", "152", "90.8", "36.6", "159", "106", "93", "Metka Rožnik");
              
              dodajMeritve(ehrId, "2018-10-15T10:00", "152", "92.8", "36.5", "162", "107", "94", "Metka Rožnik");
              dodajMeritve(ehrId, "2018-10-25T18:00", "152", "93", "36.2", "164", "108", "94", "Metka Rožnik");
              
              dodajMeritve(ehrId, "2018-12-15T10:00", "152", "94.8", "36.8", "164", "110", "93", "Metka Rožnik");
              dodajMeritve(ehrId, "2018-12-25T18:00", "152", "94.7", "37", "165", "111", "93", "Metka Rožnik");
            }
          },
          error: function(err) {
            $("#kreirajSporocilo").html("<span class='obvestilo label " +
            "label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
          }
        });
      }
    });
  }
}

function preberiPodatke() {
  var ehrId = $("#preberiEHRid").val();
  
  if (!ehrId || ehrId.trim().length == 0) {
    $("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
  } else {
    $.ajax({
      url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
    	  var party = data.party;
    	  
    	  $("#kreirajIme").val(party.firstNames);
    	  $("#kreirajPriimek").val(party.lastNames);
        var datumRojstva = party.dateOfBirth.split("-");
        $("#kreirajDan").val(datumRojstva[2]);
        $("#kreirajMesec").val(datumRojstva[1]);
        $("#kreirajLeto").val(datumRojstva[0]);
        $("#dodajVitalnoEHR").val(ehrId);
        document.getElementById("izberiBolnika").options[4].innerHTML = party.firstNames + " " + party.lastNames;
        document.getElementById("izberiBolnika").selectedIndex = 4;
        
        $("#kreirajIme").prop('readonly', true);
        $("#kreirajPriimek").prop('readonly', true);
        $("#kreirajDan").prop('readonly', true);
        $("#kreirajMesec").prop('readonly', true);
        $("#kreirajIme").prop('readonly', true);
        $("#kreirajLeto").prop('readonly', true);
        $("#dodajPodatkeOBolniku").prop('disabled', true);
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
  		}
		});
	}
}

function dodajMeritve(ehrId, datum, visina, teza, temperatura, sistolisticni, diastolisticni, nasicenost, merilec) {
	if (!ehrId || ehrId.trim().length == 0) {
	  $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datum,
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": temperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolisticni,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolisticni,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenost
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'>Meritev uspešno dodana.</span>");
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}

function preberiMeritve(vitalniZnaki) {
  $("#tabela").remove();
  $("#skrij").remove();
  if (vitalniZnaki[0].length == 0)
    $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Ni meritev.</span>");
  else {
    var meritve = "<button type='button' class='skrij active' id='skrij'><b>-</b></button><table class='table' id='tabela'>";
    for (var i = 0; i < vitalniZnaki[0].length; i++) {
      var datum = vitalniZnaki[0][i].time;
      meritve += "<tr class='vrstica-tabele'><td id='datumMeritve'>" + datum + "</td></tr>";
    }
    meritve += "</table>";
    $("#rezultatMeritve").append(meritve);
    $("#pregledMeritevVitalnihZnakov").css("display", "block");
    $("#oknoMeritev").css("display", "block");
  }
}

function pridobiVitalneZnake(callback) {
  var ehrId = $("#preberiEHRid").val();
  var rezultat = [];
  
  if (!ehrId || ehrId.trim().length == 0) {
    $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
	  $.ajax({
	    url: baseUrl + "/view/" + ehrId + "/height",
    	type: 'GET',
    	headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        var visine = [];
    	  for (var i = 0; i < res.length; i++)
    	    visine[i] = res[i];
    	  rezultat[0] = visine;
  	    $.ajax({
  			  url: baseUrl + "/view/" + ehrId + "/weight",
  			  type: 'GET',
  			  headers: {
            "Authorization": getAuthorization()
          },
          success: function (res) {
            var teze = [];
            for (var i = 0; i < res.length; i++)
              teze[i] = res[i];
            rezultat[1] = teze;
            $.ajax({
              url: baseUrl + "/view/" + ehrId + "/body_temperature",
  			      type: 'GET',
  			      headers: {
                "Authorization": getAuthorization()
              },
              success: function (res) {
                var temperature = [];
                for (var i = 0; i < res.length; i++)
                  temperature[i] = res[i];
                rezultat[2] = temperature;
                $.ajax({
                  url: baseUrl + "/view/" + ehrId + "/blood_pressure",
  			          type: 'GET',
  			          headers: {
  			            "Authorization": getAuthorization()
  			          },
  			          success: function (res) {
  			            var tlaki = [];
  			            for (var i = 0; i < res.length; i++)
                      tlaki[i] = res[i];
                    rezultat[3] = tlaki;
                    $.ajax({
                      url: baseUrl + "/view/" + ehrId + "/spO2",
                      type: 'GET',
                      headers: {
                        "Authorization": getAuthorization()
  			              },
  			              success: function (res) {
  			                var nasicenosti = [];
  			                for (var i = 0; i < res.length; i++)
  			                  nasicenosti[i] = res[i];
  			                rezultat[4] = nasicenosti;
  			                vitalniZnaki = rezultat;
  			                callback(rezultat);
  			              },
  			              error: function(err) {
  			                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                          JSON.parse(err.responseText).userMessage + "'!");
  			              }
  			            });
                  },
                  error: function(err) {
  			            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                      "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                      JSON.parse(err.responseText).userMessage + "'!");
  			          }
  			        });
              },
              error: function(err) {
  			        $("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
  			      }
  			    });
  			  },
  			  error: function(err) {
  			    $("#preberiMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "'!");
          }
  			});
    	},
    	error: function(err) {
    	  $("#preberiMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
    });
	}
}

function mesecnaPovprecja(vitalniZnaki) {
  var indeks = vitalniZnaki[3].length, prejsnji;
  for (var i = 0; i < vitalniZnaki[3].length; i++) {
    var leto = vitalniZnaki[3][i].time.substring(0, 4);
      if (i == 0) prejsnji = leto;
      if (leto != prejsnji) {
        indeks = i;
        break;
      }
      prejsnji = leto;
    }
    
    var meseci = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var sis = 0, dia = 0, st = 0;
    var mesecniTlaki = [];
    for (var i = 0; i < indeks; i++) {
      var datum = vitalniZnaki[3][i].time.substring(0, 7);
      if (i == 0) prejsnji = datum;
      if (datum != prejsnji) {
        var sisPovprecje = sis/st;
        var diaPovprecje = dia/st;
        var map = (sisPovprecje+2*diaPovprecje)/3;
        
        var mesec = vitalniZnaki[3][i-st].time.substring(0, 7);
        mesec = +mesec.substring(5);
        mesec = meseci[mesec-1];
        
        var el = {
          month: mesec,
          sys: +sisPovprecje.toFixed(2),
          dia: +diaPovprecje.toFixed(2),
          map: +map.toFixed(2)
        };
        mesecniTlaki.push(el);
        sis = 0, dia = 0, st = 0;
      }
      sis += vitalniZnaki[3][i].systolic;
      dia += vitalniZnaki[3][i].diastolic;
      st++;
      if (i == indeks-1) {
        sisPovprecje = sis/st;
        diaPovprecje = dia/st;
        map = (sisPovprecje+2*diaPovprecje)/3;
        
        mesec = vitalniZnaki[3][i-st+1].time.substring(0, 7);
        mesec = +mesec.substring(5);
        mesec = meseci[mesec-1];
        
        el = {
          month: mesec,
          sys: +sisPovprecje.toFixed(2),
          dia: +diaPovprecje.toFixed(2),
          map: +map.toFixed(2)
        };
        mesecniTlaki.push(el);
        sis = 0, dia = 0, st = 0;
      }
      prejsnji = datum;
    }
    return mesecniTlaki;
}

function procentTveganjaInPovprecja(mesecniTlaki) {
  var sis = 0, dia = 0;
  var sisProcent, diaProcent;
  for (var i = 0; i < mesecniTlaki.length; i++) {
    sis += mesecniTlaki[i].sys;
    dia += mesecniTlaki[i].dia;
  }
  sis = sis/mesecniTlaki.length;
  dia = dia/mesecniTlaki.length;
  var map = (sis+2*dia)/3;
  
  if (sis > 130 || dia > 85)
    diagnoza("hypertension", "raised", "higher", "harder");
  else if (sis < 110 || dia < 75)
    diagnoza("hypotension", "lowered", "lower", "less");
  
  $("#povprecniMAP").html("Vaš povprečni <a onclick='definicija(map)'>srednji arterijski tlak</a> je <b>" + +map.toFixed(2) + "</b> mmGh");
  $("#povprecniSistolisticni").html("Vaš povprečni <a onclick='definicija(sis)'>sistolistični</a> krvni tlak je <b>" + +sis.toFixed(2) + "</b> mmGh");
  $("#povprecniDiastolisticni").html("Vaš povprečni <a onclick='definicija(dia)'>diastolistični</a> krvni tlak je <b>" + +dia.toFixed(2) + "</b> mmGh");
  
  if (sis >= 120) {
    sis -= 120;
    sisProcent = 50*sis/90;
  } else {
    sis -= 120;
    sis = Math.abs(sis);
    sisProcent = 50*sis/70;
  }
  if (dia >= 80) {
    dia -= 80;
    diaProcent = 50*dia/70;
  } else {
    dia -= 80;
    dia = Math.abs(dia);
    diaProcent = 50*dia/50;
  }
  
  var procent = sisProcent+diaProcent;
  if (procent >= 20 && procent < 40)
    $("#procentTveganja").html("Vaš <b>procent tveganja</b> je <span style='color:orange'><b>" + +procent.toFixed(2) + "</b>%</span>");
  else if (procent >= 40)
    $("#procentTveganja").html("Vaš <b>procent tveganja</b> je <span style='color:red'><b>" + +procent.toFixed(2) + "</b>%</span>");
  else
    $("#procentTveganja").html("Vaš <b>procent tveganja</b> je <span style='color:green'><b>" + +procent.toFixed(2) + "</b>%</span>");
}

function diagnoza(beseda, a, b, c) {
  $("#diagnoza").text("...");
  $("#oknoDiagnoza").css("display", "block");
  $.ajax({
	  url: "https://googledictionaryapi.eu-gb.mybluemix.net/?define=" + beseda,
	  type: "GET",
		success: function (res) {
		  var ime = res.word;
		  ime = ime.charAt(0).toUpperCase()+ime.slice(1);
		  var opis = res.meaning.noun[0].definition;
		  opis = opis.slice(0, -1);
		  $("#diagnoza").html("<b>" + ime + "</b>, also known as " + opis + 
		             ", is a condition in which the blood vessels have persistently " + a + " pressure. " +
		             "Blood is carried from the heart to all parts of the body in the vessels. Each time" +
		             " the heart beats, it pumps blood into the vessels. Blood pressure is created by the" +
		             " force of the blood pushing against the walls of blood vessels (arteries) as it is " +
		             "pumped by the heart. The " + b + " the pressure the " + c + " the heart has to pump.");
		}
	});
}

function definicija(beseda) {
  if (beseda == "map") {
    $("#oknoDefinicija").css("display", "block");
    $("#definicija").html("<b>mean arterial pressure</b>: the average arterial pressure throughout one cardiac cycle.");
  } else {
    $("#definicija").text("...");
    $("#oknoDefinicija").css("display", "block");
    $.ajax({
	    url: "https://googledictionaryapi.eu-gb.mybluemix.net/?define=" + beseda,
	    type: "GET",
		  success: function (res) {
		    $("#definicija").html("<b>" + res.word + "</b>: " + res.meaning.adjective[0].definition);
		  }
	  });
  }
}

$(document).ready(function() {
  
  $("#generirajPodatke").click(function() {
    $(".obvestilo").remove();
    generirajPodatke(1);
    generirajPodatke(2);
    generirajPodatke(3);
  });
  
  $("#izberiBolnika").change(function() {
    $(".obvestilo").remove();
    
    if (document.getElementById("izberiBolnika").selectedIndex == 0) {
      $("#preberiEHRid").val("");
      $("#kreirajIme").val("");
      $("#kreirajPriimek").val("");
      $("#kreirajDan").val("");
      $("#kreirajMesec").val("");
      $("#kreirajLeto").val("");
      $("#dodajVitalnoEHR").val("");
      document.getElementById("izberiBolnika").options[4].innerHTML = "Drug bolnik";
      
      $("#kreirajIme").prop('readonly', false);
      $("#kreirajPriimek").prop('readonly', false);
      $("#kreirajDan").prop('readonly', false);
      $("#kreirajMesec").prop('readonly', false);
      $("#kreirajIme").prop('readonly', false);
      $("#kreirajLeto").prop('readonly', false);
      $("#dodajPodatkeOBolniku").prop('disabled', false);
      
      $("#pregledMeritevVitalnihZnakov").css("display", "none");
      $("#oknoMeritev").css("display", "none");
      $("#oknoPodrobnosti").css("display", "none");
      $("#grafInDiagnoza").css("display", "none");
    } else if (document.getElementById("izberiBolnika").selectedIndex == 4) {
      $("#dodajVitalnoEHR").val($("#preberiEHRid").val);
    } else {
      $("#preberiEHRid").val($("#bolnik" + document.getElementById("izberiBolnika").selectedIndex).val());
      document.getElementById("izberiBolnika").options[4].innerHTML = "Drug bolnik";
    }
  });
  
  $("#preberiPodatkeOBolniku").click(function() {
    $(".obvestilo").remove();
    
    //$("#tabela").remove();
    //$("#skrij").remove();
    $("#oknoPodrobnosti").css("display", "none");
    $("#grafInDiagnoza").css("display", "none");
    $("#definicija").text("");
    $("#oknoDefinicija").css("display", "none");
    $("#diagnoza").text("");
    $("#oknoDiagnoza").css("display", "none");
    
    preberiPodatke();
    pridobiVitalneZnake(function(vitalniZnaki) {
      preberiMeritve(vitalniZnaki);
      var mesecniTlaki = mesecnaPovprecja(vitalniZnaki);
      if (vitalniZnaki[3].length > 0) $("#grafInDiagnoza").css("display", "block");
      narisiGraf(mesecniTlaki);
      procentTveganjaInPovprecja(mesecniTlaki);
    });
  });
  
  $("#dodajPodatkeOBolniku").click(function() {
    $(".obvestilo").remove();
    generirajPodatke(4);
  });
  
  $("#dodajMeritveVitalnihZnakov").click(function() {
    $(".obvestilo").remove();
    
    var ehrId = $("#dodajVitalnoEHR").val();
    var datum = $("#dodajVitalnoDatumInUra").val();
    var visina = $("#dodajVitalnoTelesnaVisina").val();
    var teza = $("#dodajVitalnoTelesnaTeza").val();
    var temperatura = $("#dodajVitalnoTelesnaTemperatura").val();
    var sistolisticni = $("#dodajVitalnoKrvniTlakSistolisticni").val();
    var diastolisticni = $("#dodajVitalnoKrvniTlakDiastolisticni").val();
    var nasicenost = $("#dodajVitalnoNasicenostKrviSKisikom").val();
    var merilec = $("#dodajVitalnoMerilec").val();
    dodajMeritve(ehrId, datum, visina, teza, temperatura, sistolisticni, diastolisticni, nasicenost, merilec);
    
    //$("#tabela").remove();
    //$("#skrij").remove();
    $("#oknoPodrobnosti").css("display", "none");
    $("#grafInDiagnoza").css("display", "block");
    $("#pregledMeritevVitalnihZnakov").css("display", "none");
    $("#oknoMeritev").css("display", "none");
    $("#oknoPodrobnosti").css("display", "none");
    $("#definicija").text("");
    $("#oknoDefinicija").css("display", "none");
    $("#diagnoza").text("");
    $("#oknoDiagnoza").css("display", "none");
    if ($("#tabela").length) {
      $("#tabela").remove();
      $("#skrij").remove();
    }
    
    setTimeout(function() {
      pridobiVitalneZnake(function(vitalniZnaki) {
        preberiMeritve(vitalniZnaki);
        var mesecniTlaki = mesecnaPovprecja(vitalniZnaki);
        narisiGraf(mesecniTlaki);
        procentTveganjaInPovprecja(mesecniTlaki);
      });
    }, 1000);
  });
  
  $(document).on('click', '.vrstica-tabele', function() {
    $("#tabela tr.vrstica-tabele").removeClass("izbrana-vrstica");
    $(this).addClass("izbrana-vrstica");
    
    var datum = this.innerHTML.split('>');
    datum = datum[1].split('<');
    datum = datum[0];
    for (var i = 0; i < vitalniZnaki[0].length; i++) {
      if (datum == vitalniZnaki[0][i].time) {
        $("#podrobnostiVisina").html("<b>Telesna višina:</b> " + vitalniZnaki[0][i].height + " " + vitalniZnaki[0][i].unit);
        $("#podrobnostiTeza").html("<b>Telesna teža:</b> " + vitalniZnaki[1][i].weight + " " + vitalniZnaki[1][i].unit);
        $("#podrobnostiTemperatura").html("<b>Telesna temperatura:</b> " + vitalniZnaki[2][i].temperature + " " + vitalniZnaki[2][i].unit);
        $("#podrobnostiSistolisticni").html("<b>Sistolistični krvni tlak:</b> " + vitalniZnaki[3][i].systolic + " " + vitalniZnaki[3][i].unit);
        $("#podrobnostiDiastolisticni").html("<b>Diastolistični krvni tlak:</b> " + vitalniZnaki[3][i].diastolic + " " + vitalniZnaki[3][i].unit);
        $("#podrobnostiNasicenost").html("<b>Nasičenost krvi s kisikom:</b> " + vitalniZnaki[4][i].spO2 + "%");
      }
    }
    //$("#pregledMeritevVitalnihZnakov").css("display", "block");
    //$("#graf").css("display", "block");
    $("#oknoPodrobnosti").css("display", "block");
  });
  
  $(document).on("click", ".skrij", function() {
    this.classList.toggle("active");
    if ($("#tabela:visible").length == 0) {
      $("#tabela").css("display", "table");
      $("#skrij").html("<b>+</b>");
    } else {
      $("#tabela").css("display", "none");
      $("#skrij").html("<b>-</b>");
      $("#oknoPodrobnosti").css("display", "none");
    }
  });
  
});